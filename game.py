from random import randint


name = input("Hi, What is your name?")

for guesses in range(5):
    month = randint(1, 12)
    year = randint(1950,2022)
    print("Guess #", guesses + 1, name, "is", month, "/", year, "your birthday?" )
    answer = input("Did I get it right? (yes or no)")
    if answer == "yes":
        print("Yay!")
        exit()
    elif answer == "no":
        print("Boo, I'll guess again")


print("I guess I couldn't get it. You win.")
